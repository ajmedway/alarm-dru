<?php

/**
 * Copyright © 2018 r//evolution Marketing. All rights reserved.
 * 
 * alarm_membership module
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * @file
 * Defines functions that run custom database-layer queries for the
 * alarm_membership module.
 */

/**
 * Get licenses for memberships due to expire after a specified time interval
 * 
 * @param string $interval
 */
function get_members_due_to_expire_after_x_interval($interval) {
  $query = "
        SELECT *
          FROM commerce_license cl
         WHERE `type` = :license_type
           AND DATE(FROM_UNIXTIME(`expires`)) = DATE(DATE_ADD(NOW(), {$interval}))";
  $results = db_query($query, [':license_type' => 'role']);

  return ($results->rowCount() > 0) ? $results : [];
}
