<?php

/**
 * Copyright © 2018 r//evolution Marketing. All rights reserved.
 * 
 * alarm_membership module
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * @file
 * Defines generic reusable helper functions for the alarm_membership module.
 */

/**
 * Get all members via EntityFieldQuery. Optionally chunk the results returned.
 * 
 * @param int $chunk_size
 * @param int $chunk_idx
 * @return array List of members (not full objects).
 */
function get_all_members($chunk_size = 0, $chunk_idx = 0) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'content_member');
      //->fieldCondition('field_work_email', 'value', '', '!=');
  $members = $query->execute();

  if (!empty($members['node'])) {
    if (!empty($chunk_size)) {
      $chunk_idx = (int) $chunk_idx;
      $member_chunks = array_chunk($members['node'], $chunk_size);
      $members = !empty($member_chunks[$chunk_idx]) ? $member_chunks[$chunk_idx] : [];
      return $members;
    }

    return $members['node'];
  }

  return [];
}

/**
 * Get member associated with a user.
 * 
 * @param object $user
 * @return object|bool A fully-loaded $member object upon successful retrieval or false if not found.
 */
function get_member_for_user($user) {
  if (!empty($user->uid)) {
    $member = get_member_by_user_id($user->uid);
    return $member;
  }

  return false;
}

/**
 * Get member by user ID.
 * 
 * @param int $user_id
 * @return object|bool A fully-loaded $member object upon successful retrieval
 * or false if not found.
 */
function get_member_by_user_id($user_id) {
  if (!empty($user_id)) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'content_member')
        ->fieldCondition('field_user', 'target_id', $user_id, '=')
        ->propertyOrderBy('created', 'DESC')
        ->range(0, 1);
    $result = $query->execute();

    if (!empty($result['node'])) {
      $nids = array_keys($result['node']);
      if (!empty($nids[0])) {
        $member = node_load($nids[0]);
        return $member;
      }
    }
  }

  return false;
}

/**
 * Get member by node ID.
 * 
 * @param int $node_id
 * @return object|bool A fully-loaded $member object upon successful retrieval
 * or false if not found.
 */
function get_member_by_node_id($node_id) {
  if (!empty($node_id)) {
    $member = node_load($node_id);
    return $member;
  }

  return false;
}

/**
 * Get latest/highest existing member number.
 * 
 * @return int Highest/latest member number or false if not found.
 */
function get_latest_member_number() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'content_member')
      ->fieldOrderBy('field_member_number', 'value', 'DESC')
      ->range(0, 1);
  $result = $query->execute();

  if (!empty($result['node'])) {
    $nids = array_keys($result['node']);
    if (!empty($nids[0])) {
      $member = node_load($nids[0]);
      // get entity wrapper for member
      $member_wrapper = entity_metadata_wrapper('node', $member);
      return $member_wrapper->field_member_number->value();
    }
  }

  return false;
}

/**
 * Get next available member number.
 * 
 * @return int Next available member number or false if not found.
 */
function get_next_available_member_number() {
  $latest_member_number = get_latest_member_number();
  $latest_member_number++;
  
  // ensure newly approved members are assigned numbers staring from 20000
  /*if ($latest_member_number < 20000) {
    $latest_member_number = 20000;
  }*/
  
  return $latest_member_number;
}

/**
 * Get members associated with a lead member
 * 
 * @param object $lead_member
 * @return object[]|bool An array of fully-loaded $member objects upon successful retrieval or false if none found.
 */
function get_associated_members_of_lead_member($lead_member) {
  if (!empty($lead_member)) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'content_member')
        ->fieldCondition('field_lead_member_profile', 'target_id', $lead_member->nid, '=');
    $result = $query->execute();

    if (!empty($result['node'])) {
      $nids = array_keys($result['node']);

      if (!empty($nids)) {
        $members = node_load_multiple($nids, ['type' => 'content_member']);
        return $members;
      }
    }
  }

  return false;
}

/**
 * Create (or get existing) user and associate member record
 * 
 * @param object $member
 * @param string $email
 * @return object|bool A fully-loaded $user object upon successful save or false if the save failed.
 */
function make_user_for_member($member, $email) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);

  // use native drupal functions to slugify first name, last name and member nid, which will be simple and unique
  $rawUsernameString = $member_wrapper->field_first_name->value() . $member_wrapper->field_last_name->value() . $member_wrapper->nid->value();
  $username = get_slug($rawUsernameString);

  // build basic user data object
  $userData = (object) [
        'name' => $username,
        'mail' => $email,
        'status' => true,
        'is_new' => true,
        'timezone' => variable_get('date_default_timezone', ''),
  ];

  // if the user already exists, leave it undisturbed and get user object
  if (($user = user_load_by_mail($email)) === false) {
    // otherwise create new user record
    $user = user_save($userData);
  }

  return $user;
}

/**
 * Get the referenced user object for a member.
 * 
 * @param object $member
 * @param int $user_id
 * @return object|bool A fully-loaded $user object upon successful save or false if the save failed.
 */
function get_user_for_member($member) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  // get the user
  $user = $member_wrapper->field_user->value() ?: false;
  
  return $user;
}

/**
 * Link the member record to user account via Entity Reference field.
 * 
 * @param object $member
 * @param int $user_id
 * @return void
 */
function associate_member_with_user($member, $user_id) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  // set the user_id
  $member_wrapper->field_user->set((int) $user_id);
  // additionally set the member author as this user
  $member_wrapper->author->set((int) $user_id);
  $member_wrapper->save();
}

/**
 * Set as lead member profile
 * 
 * @param object $member
 * @param int $user_id
 * @return void
 */
function set_member_as_lead($member) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  // set the field_lead_member to true
  $member_wrapper->field_lead_member->set(true);
  $member_wrapper->save();
}

/**
 * Link the member record to designated lead member profile via Entity Reference
 * field.
 * 
 * @param object $member
 * @param int $lead_member_id
 * @return void
 */
function set_lead_member_reference($member, $lead_member_id) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  // set the field_lead_member_profile to the lead member node id
  $member_wrapper->field_lead_member_profile->set($lead_member_id);
  $member_wrapper->save();
}

/**
 * Link the member record to a membership type via Term Reference field.
 * 
 * @param object $member
 * @param int $membership_type_tid
 * @return void
 */
function set_membership_type_reference($member, $membership_type_tid) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  // set the field_lead_member_profile to true
  $member_wrapper->field_membership_type->set($membership_type_tid);
  $member_wrapper->save();
}

/**
 * @param object $user
 * @param string $role_name
 * @return bool success
 */
function add_user_to_role_by_role_name($user, $role_name) {
  $role = user_role_load_by_name($role_name);
  if (!empty($role)) {
    user_multiple_role_edit([$user->uid], 'add_role', $role->rid);
    return true;
  }
  return false;
}

/**
 * @param object $user
 * @param int $role_id
 * @return bool success
 */
function add_user_to_role_by_role_id($user, $role_id) {
  $role = user_role_load($role_id);
  if (!empty($role)) {
    user_multiple_role_edit([$user->uid], 'add_role', $role->rid);
    return true;
  }
  return false;
}

/**
 * @param object $user
 * @param string $role_name
 * @return bool success
 */
function remove_user_from_role_by_role_name($user, $role_name) {
  $role = user_role_load_by_name($role_name);
  if (!empty($role)) {
    user_multiple_role_edit([$user->uid], 'remove_role', $role->rid);
    return true;
  }
  return false;
}

/**
 * @param object $user
 * @param int $role_id
 * @return bool success
 */
function remove_user_from_role_by_role_id($user, $role_id) {
  $role = user_role_load($role_id);
  if (!empty($role)) {
    user_multiple_role_edit([$user->uid], 'remove_role', $role->rid);
    return true;
  }
  return false;
}

/**
 * Use native drupal functions to slugify any string.
 * 
 * @param string $raw_string
 * @return string slug
 */
function get_slug($raw_string = '') {
  return drupal_html_class(
      drupal_clean_css_identifier($raw_string)
  );
}

/**
 * Get all current membership products.
 * 
 * @return array A basic product data array (not full product objects)
 */
function get_all_membership_products() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_product')
      ->propertyCondition('type', 'membership')
      ->propertyCondition('status', NODE_PUBLISHED);
  $products = $query->execute();

  return $products;
}

/**
 * Get the appropriate membership product for role ID and member group total.
 * 
 * @param int $role_id
 * @param int $total_members_in_lead_group
 * @return object|bool A fully-loaded product object upon successful retrieval or false if none found.
 */
function get_membership_product_for_role_and_member_total($role_id, $total_members_in_lead_group) {
  if (!empty($role_id) && !empty($total_members_in_lead_group)) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_product')
        ->propertyCondition('type', 'membership')
        ->propertyCondition('status', NODE_PUBLISHED)
        ->propertyCondition('product_id', 30, '!=') // exclude renewal product
        ->fieldCondition('commerce_license_type', 'value', 'role')
        ->fieldCondition('commerce_license_role', 'value', $role_id)
        ->fieldCondition('field_member_quantity', 'value', $total_members_in_lead_group)
        // sort by oldest first so that this function cherry-picks oldest if
        // there are multiple membership products meeting same criteria - the
        // new ones may be just test products and we want to use the originals,
        // which should not really change.
        ->propertyOrderBy('created', 'ASC');
    $products = $query->execute();

    if (!empty($products['commerce_product'])) {
      $product_ids = array_keys($products['commerce_product']);
      if (!empty($product_ids[0])) {
        $product = commerce_product_load($product_ids[0]);
        return $product;
      }
    }
  }

  return false;
}

/**
 * Get most recent membership license successfully purchased by user.
 * 
 * @param object $user
 * @param bool $get_active_only
 * @return object|bool A fully-loaded product object upon successful retrieval or false if none found.
 */
function get_membership_license_for_user($user, $get_active_only = false) {

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_license')
      ->propertyCondition('uid', $user->uid)
      ->propertyCondition('type', 'role')
      ->propertyOrderBy('expires', 'DESC')
      ->range(0, 1);

  if ($get_active_only) {
    $query->propertyCondition('status', COMMERCE_LICENSE_ACTIVE);
  } else {
    // ensure this license is active or expired, i.e. only licenses that were
    // fully and successfully purchased
    $query->propertyCondition('status', COMMERCE_LICENSE_PENDING, '>');
  }

  $result = $query->execute();

  // get the license
  if (!empty($result['commerce_license'])) {
    $result_license_ids = array_keys($result['commerce_license']);
    $current_id = array_shift($result_license_ids);
    $license = entity_load_single('commerce_license', $current_id);
    return $license;
  }

  return false;
}

/**
 * Get most recent membership license product successfully purchased by user.
 * 
 * @param object $user
 * @param bool $get_active_only
 * @return object|bool A fully-loaded product object upon successful retrieval or false if none found.
 */
function get_membership_license_product_for_user($user, $get_active_only = false) {

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_license')
      ->propertyCondition('uid', $user->uid)
      ->propertyCondition('type', 'role')
      ->propertyOrderBy('expires', 'DESC')
      ->range(0, 1);

  if ($get_active_only) {
    $query->propertyCondition('status', COMMERCE_LICENSE_ACTIVE);
  } else {
    // ensure this license is active or expired, i.e. only licenses that were
    // fully and successfully purchased
    $query->propertyCondition('status', COMMERCE_LICENSE_PENDING, '>');
  }

  $result = $query->execute();

  // get the license
  if (!empty($result['commerce_license'])) {
    $result_license_ids = array_keys($result['commerce_license']);
    $current_id = array_shift($result_license_ids);
    $license = entity_load_single('commerce_license', $current_id);
    
    // check if this is a full membership product/if the user has a different
    // number of members since they made their purchase (due to members being
    // manually cancelled/reassigned etc.)
    $full_member_role = user_role_load_by_name('full member');
    $license_product_role = (int) $license->wrapper->product->commerce_license_role->value();
    $full_member_role_id = (int) $full_member_role->rid;
    if ($license_product_role === $full_member_role_id) {
      // get the lead member profile and associated members
      $member = get_member_for_user($user);
      $associated_members = get_associated_members_of_lead_member($member);
      
      // get the current total number of members in this lead group
      $total_members_in_lead_group = 1;
      if (!empty($associated_members)) {
        $total_members_in_lead_group += count($associated_members);
      }
      
      // if needed, return different renewal product to previous one purchased
      if ((int) $license->wrapper->product->field_member_quantity->value() !== $total_members_in_lead_group) {
        $changed_license_product = get_membership_product_for_role_and_member_total($full_member_role_id, $total_members_in_lead_group);
        if (!empty($changed_license_product)) {
          return $changed_license_product;
        }
      }
    }
    
    return $license->wrapper->product->value();
  }

  return false;
}
