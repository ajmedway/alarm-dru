<?php

/**
 * Copyright © 2018 r//evolution Marketing. All rights reserved.
 * 
 * alarm_membership module
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * @file
 * Defines custom mailer functionality and mailers for the alarm_membership
 * module.
 */

/**
 * Implements hook_mail().
 * 
 * @link https://www.drupal.org/forum/support/module-development-and-code-questions/2015-04-09/learn-how-to-send-e-mails
 */
function alarm_membership_mail($key, &$message, $params) {
  // check mail key and handle message params uniquely, if needed
  switch ($key) {
    case 'new_member_welcome':
    case 'membership_expiry_warning':
    default:
      $message['subject'] = $params['subject'];
      $message['body'][] = $params['body'];
      $message['headers']['MIME-Version'] = '1.0';
      $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
      $message['headers']['Content-Transfer-Encoding'] = '8bit';
      $message['headers']['X-Mailer'] = 'Drupal';
      break;
  }
}

/**
 * Sends out membership expiry warning emails.
 * 
 * @param array $intervals A set of MySQL INTERVAL strings mapping to labels:
 * 
 * $intervals = [
 *   'INTERVAL 1 WEEK' => '1 week',
 *   'INTERVAL 2 WEEK' => '2 weeks',
 *   'INTERVAL 30 DAY' => '1 month',
 *   'INTERVAL 60 DAY' => '2 months',
 *   'INTERVAL 90 DAY' => '3 months',
 * ];
 * 
 * @param bool $dry_run
 * 
 * @return int|bool Total emails sent or false on failure
 */
function send_membership_expiry_warning_emails($intervals = [], $dry_run = false) {
  $total_emails = 0;

  if (is_array($intervals) && !empty($intervals)) {
    foreach ($intervals as $interval => $time_remaining_label) {
      $license_results = get_members_due_to_expire_after_x_interval($interval);
      foreach ($license_results as $license) {
        $user = user_load($license->uid);
        $member = get_member_for_user($user);
        // get entity wrapper for member
        $member_wrapper = entity_metadata_wrapper('node', $member);
        
        // flag for eligibility to receive the expiry warning email
        // sponsor and honorary members are not eligible
        $ineligible = ($member_wrapper->field_honorary_member->value() || $member_wrapper->field_sponsor_member->value());
        if (!$ineligible) {
          $total_emails++;
        }
        
        if (!$dry_run && !$ineligible) {
          send_alarm_membership_expiry_warning_mail($member, $time_remaining_label);
        }
      }
    }
  }

  return $total_emails;
}

/**
 * Send out the ALARM membership welcome mail to newly approved members.
 * 
 * @param object $member
 * @return void
 */
function send_alarm_membership_welcome_mail($member) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  $user = $member_wrapper->field_user->value();
  $pass_reset_url = user_pass_reset_url($user);
  $member_user_email = $member_wrapper->field_user->mail->value();
  $member_firstname = $member_wrapper->field_first_name->value();
  $message = "Hello {$member_firstname},

You have been successfully registered and approved as a member of ALARM.

Your username is: {$user->name}

Please click on the link below within the next 24 hours to create a password and to access your account:

{$pass_reset_url}

If you have any problems signing in, please contact a member of our team by email: [site:mail]

Many thanks, [site:name] team
";

  // replace tokens
  $final_message = token_replace($message, array('user' => $user));

  // send the email
  $params = array(
    'subject' => t("Welcome to ALARM, {$member_firstname}"),
    'body' => check_markup(
        t($final_message), 'full_html'
    ),
  );
  $sent_message = drupal_mail('alarm_membership', 'new_member_welcome', $member_user_email, language_default(), $params);
  watchdog('alarm_membership_welcome_mail', 'Sent email to %recipient', array('%recipient' => $member_user_email));
}

/**
 * Send out the ALARM membership expiry warning mail.
 * 
 * @param object $member
 * @param string $time_remaining_label
 * @return void
 */
function send_alarm_membership_expiry_warning_mail($member, $time_remaining_label) {
  // get entity wrapper for member
  $member_wrapper = entity_metadata_wrapper('node', $member);
  $user = $member_wrapper->field_user->value();
  $member_user_email = $member_wrapper->title->value();
  $member_firstname = $member_wrapper->field_first_name->value();
  $message = "Hello {$member_firstname},

Please be aware that your ALARM membership is due to expire in {$time_remaining_label}.

Please sign into your account (follow link below) and click on the renewal link.

[site:url]user

If you have any problems signing in and renewing your membership, please contact a member of our team by email: [site:mail]

Many thanks, [site:name] team
";

  // replace tokens
  $final_message = token_replace($message, array('user' => $user));

  // send the email
  $params = array(
    'subject' => t("It's time to renew your ALARM membership"),
    'body' => check_markup(
        t($final_message), 'full_html'
    ),
  );
  $sent_message = drupal_mail('alarm_membership', 'membership_expiry_warning', $member_user_email, language_default(), $params);
  watchdog('alarm_membership_expiry_warning_mail', 'Sent email to %recipient', array('%recipient' => $member_user_email));
}
