<?php

	if(isset($content['body']))
		print render($content['body']);

	if(isset($content['field_downloads_listing']))
		print '<h3>Resources</h3>'.render($content['field_downloads_listing']);

	if(isset($content['field_downloads']))
		print '<h3>Downloads</h3>'.render($content['field_downloads']);

?>

<?php
module_load_include('inc', 'user', 'user.pages');
global $user;
$form = drupal_get_form('user_profile_form', user_load($user->uid));
print(drupal_render($form));
?>

<?php $field_gallery_items = field_get_items('node', $node, 'field_gallery'); ?>

<?php if(!empty($field_gallery_items)) : ?>

	<h3>Gallery</h3>

	<div class="gallery">

	<?php foreach($field_gallery_items as $item) : $imgUrl = file_create_url($item['uri']); ?>
		<a class="container" data-fancybox="gallery" href="<?php echo $imgUrl; ?>">
			<div class="image" style="background: url('<?php echo $imgUrl; ?>') center no-repeat;background-size:cover;"></div>
		</a>
	<?php endforeach; ?>

	</div>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>

<?php endif; ?>

<?php

	if(isset($content['field_google_map_address']))
		print render($content['field_google_map_address']);

	if(isset($content['field_youtube_video']))
		print render($content['field_youtube_video']);

?>