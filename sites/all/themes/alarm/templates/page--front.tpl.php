<?php $page['content']['system_main']['default_message'] = array(); ?>

<nav id="main_nav">
	<div class="wrapper">
		
		<a class="logo" href="/"><img src="<?php echo base_path().path_to_theme(); ?>/img/logo.png" alt="ALARM" title="ALARM">ALARM</a>
		<div id="menu_blocks">
			<?php print render($page['main_menu']); ?>
			<button class="search_btn">
				<?php print render($page['search']); ?>
			</button>
		</div>
<?php if (user_is_logged_in()) { 
		echo '<a class="login_btn" href="/user/logout">Log out</a>';
}
else {
		echo '<a class="login_btn" href="/user">Login</a>';
}; ?>
		<div class="menu_btn">
			<div class="bar"></div>
			<div class="bar"></div>
			<div class="bar"></div>
			Menu
		</div>

	</div>
</nav>

<div id="menu_overlay">

	<div class="navigation">
	
		<a class="logo" href="/"><img src="<?php echo base_path().path_to_theme(); ?>/img/logo_white.png" alt="ALARM" title="ALARM">ALARM</a>

		<div id="menu_blocks_mobile">
			<?php print render($page['main_menu']); ?>
			<button class="search_btn">
				<?php print render($page['search']); ?>
			</button>
		</div>

		<a class="login_btn" href="/user">Login</a>
		
		<div class="menu_btn">
			<div class="bar"></div>
			<div class="bar"></div>
			<div class="bar"></div>
			Menu
		</div>

	</div>

	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>

</div>

<section id="main_slider">
	<div class="images">
		<?php print render($page['slider_images']); ?>
	</div>
	<div class="info">
		<div class="boxes">
			<?php print render($page['slider_content']); ?>
		</div>
		<div class="ticker"></div>
	</div>
</section>

<div class="message">
<?php print render($messages); ?>
</div>

<div class="hp-content"><?php print render($page['content']); ?></div>

<section id="cta_1" class="cta right">
	<?php print render($page['cta_1']); ?>
</section>

<section id="main_listing">
		
	<?php if(!empty($page['listing_events'])) : ?>
		<div id="events_listing" class="listing">
			<h2>Upcoming Events</h2>
			<div class="list">
				<?php print render($page['listing_events']); ?>
			</div>
			<a class="btn view_all" href="events">View All Events</a>
		</div>
	<?php endif; ?>

	<?php if(!empty($page['listing_news'])) : ?>
		<div id="news_listing" class="listing">
			<h2>Latest News</h2>
			<div class="list">
				<?php print render($page['listing_news']); ?>
			</div>
			<a class="btn view_all" href="news">View All Stories</a>
		</div>
	<?php endif; ?>

	<?php if(!empty($page['listing_publications'])) : ?>
		<div id="publications_listing" class="listing">
			<h2>Latest Publications</h2>
			<div class="list">
				<?php print render($page['listing_publications']); ?>
			</div>
			<a class="btn view_all" href="shop">View All Publications</a>
		</div>
	<?php endif; ?>

</section>

<section id="cta_2" class="cta left">
	<?php print render($page['cta_2']); ?>
</section>

<?php if(!empty($page['listing_focus_groups'])) : ?>
	<section id="main_focus_groups" class="listing">
		<div id="focus_groups_listing">
			<div class="list">
				<?php print render($page['listing_focus_groups']); ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<section id="cta_3">
	<div class="wrapper">	
		<?php print render($page['cta_3']); ?>
	</div>
</section>

<section id="email_signup" class="cta left">
	<div class="text">
		<h2>Stay up to date</h2>
		<p>Here at ALARM we would like to contact you with details of events and services we provide.</p>
		<p class="small">We will not pass your details onto any other companies. Please tick the following box to give us consent to contact you for these purposes by email.</p>
		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup">
			<form action="https://alarm-uk.us15.list-manage.com/subscribe/post?u=2dcc0ad9ad62b60e51cb6e8c2&amp;id=b4b0b652db" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
			    <div id="mc_embed_signup_scroll">
					<div class="agree">
						<div class="mc-field-group input-group">
						    <input type="checkbox" value="1" name="group[7597][1]" id="mce-group[7597]-7597-0" required>
						    <label for="mce-group[7597]-7597-0">I agree</label>
						</div>
					</div>
			    	<div class="signup">
						<div class="mc-field-group">
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email address" required>
							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_2dcc0ad9ad62b60e51cb6e8c2_b4b0b652db" tabindex="-1" value=""></div>
						    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Submit</button>
						</div>
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>
			    </div>
			</form>
		</div>
		<!--End mc_embed_signup-->
	</div>
	<div class="twitter">
		<a class="twitter-timeline" data-theme="light" data-link-color="#00a1e0" href="https://twitter.com/alarmrisk?ref_src=twsrc%5Etfw">Tweets by riskalarm</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	</div>
</section>

<footer id="main_footer">
	<div class="wrapper">

		<div class="left side">
			<a class="logo" href="/"><img src="<?php echo base_path().path_to_theme(); ?>/img/logo_white.png" alt="ALARM" title="ALARM">ALARM</a>
			<ul class="social">
				<li><a class="fb" href="https://www.facebook.com/alarmrisk/" target="_blank">Facebook</a></li>
				<li><a class="tw" href="https://twitter.com/ALARMrisk" target="_blank">Twitter</a></li>
				<li><a class="in" href="https://www.linkedin.com/company/alarmrisk/" target="_blank">LinkedIn</a></li>
				<li><a class="ig" href="https://www.instagram.com/alarmrisk" target="_blank">Instagram</a></li>
			</ul>
		</div>

		<div class="right side">
			<?php print render($page['footer_menu']); ?>
			<p>Design &amp; Build by <a href="https://www.r-evolution.co.uk/" target=-"_blank">r//evolution</a> <span>ALARM &copy; 2018</span></p>
		</div>

	</div>
</footer>

<section id="partners">
	<div class="wrapper">
		<div class="list">
			<h3>Our Platinum Sponsors</h3>
			<?php print render($page['footer_sponsors']); ?>
		</div>
	</div>
</section>