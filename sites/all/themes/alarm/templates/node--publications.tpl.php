<h3><?php print render($content['field_intro_text']); ?></h3>
<?php print render($content['body']); ?>
<div class="publication_preview">
	<?php print render($content['field_publication_cover']); ?>
	<div class="sample_box">
		<a class="btn" href="<?php print render($content['field_sample_download'][0]['#markup']); ?>" target="_blank">View Free Sample</a>
	</div>
</div>