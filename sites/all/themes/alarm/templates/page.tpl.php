<?php $page['content']['system_main']['default_message'] = array(); ?>

<nav id="main_nav">
	<div class="wrapper">
		
		<a class="logo" href="/"><img src="<?php echo base_path().path_to_theme(); ?>/img/logo.png" alt="ALARM" title="ALARM">ALARM</a>
		<div id="menu_blocks">
			<?php print render($page['main_menu']); ?>
			<button class="search_btn">
				<?php print render($page['search']); ?>
			</button>
		</div>
<?php if (user_is_logged_in()) { 
		echo '<a class="login_btn" href="/user/logout">Log out</a>';
}
else {
		echo '<a class="login_btn" href="/user">Login</a>';
}; ?>
		<div class="menu_btn">
			<div class="bar"></div>
			<div class="bar"></div>
			<div class="bar"></div>
			Menu
		</div>

	</div>
</nav>

<div id="menu_overlay">

	<div class="navigation">
	
		<a class="logo" href="/"><img src="<?php echo base_path().path_to_theme(); ?>/img/logo_white.png" alt="ALARM" title="ALARM">ALARM</a>

		<div id="menu_blocks_mobile">
			<?php print render($page['main_menu']); ?>
			<button class="search_btn">
				<?php print render($page['search']); ?>
			</button>
		</div>

		<a class="login_btn" href="/user">Login</a>

		<div class="menu_btn">
			<div class="bar"></div>
			<div class="bar"></div>
			<div class="bar"></div>
			Menu
		</div>

	</div>

	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>
	<div class="layer"></div>

</div>

<section id="main_header">
	<?php print render($page['header']); ?>
</section>

<section id="main_content">
	<div class="wrapper">

		<?php if(!empty($page['side_content'])) : ?>
			<div class="main_col">
		<?php else : ?>
			<div class="main_col full_width">
		<?php endif; ?>
			
			<?php print render($tabs); ?>

			<div class="body">
				
				<?php print render($messages); ?>
				<?php print render($page['content']); ?>
				
				<?php if(!empty($page['sponsor_tiers'])) : ?>
					<div class="sponsor_tiers">
						<h3>Our Sponsors</h3> 
						<?php print render($page['sponsor_tiers']); ?>
					</div>
				<?php endif; ?>
				
				<?php if(!empty($page['pricing'])) : ?>
					<div class="pricing_table <?= !empty($node->field_external_event['und'][0]['value']) ? 'external_event' : '' ?>">
						<?php print render($page['pricing']); ?>
					</div>
				<?php endif; ?>
				
			</div>

		</div>

		<?php if(!empty($page['side_content'])) : ?>
			<div class="side_col">
				<?php print render($page['side_content']); ?>
			</div>
		<?php endif; ?>

	</div>
</section>

<?php if(!empty($page['listing_events']) || !empty($page['listing_news']) || !empty($page['listing_publications']) || !empty($page['listing_memberships']) || !empty($page['listing_jobs']) || !empty($page['listing_people']) || !empty($page['listing_members_search']) || !empty($page['listing_resource_pages'])) : ?>
	<section id="main_listing">	

		<?php if(!empty($page['listing_members_search'])) : ?>
			<div id="members_search_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_members_search']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_news'])) : ?>
			<div id="news_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_news']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_events'])) : ?>
			<div id="events_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_events']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_publications'])) : ?>
			<div id="publications_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_publications']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_memberships'])) : ?>
			<div id="memberships_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_memberships']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_jobs'])) : ?>
			<div id="jobs_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_jobs']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_people'])) : ?>
			<div id="people_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_people']); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if(!empty($page['listing_resource_pages'])) : ?>
			<div id="resource_pages_listing" class="listing">
				<div class="list">
					<?php print render($page['listing_resource_pages']); ?>
				</div>
			</div>
		<?php endif; ?>		

	</section>
<?php endif; ?>

<?php if(!empty($page['gallery'])) : ?>
	<section id="main_gallery">
		<div class="images">
			<?php print render($page['gallery']); ?>
		</div>
	</section>
<?php endif; ?>

<section id="cta_3">
	<div class="wrapper">	
		<?php print render($page['cta_3']); ?>
	</div>
</section>

<section id="email_signup" class="cta left">
	<div class="text">
		<h2>Stay up to date</h2>
		<p>Here at ALARM we would like to contact you with details of events and services we provide.</p>
		<p class="small">We will not pass your details onto any other companies. Please tick the following box to give us consent to contact you for these purposes by email.</p>
		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup">
			<form action="https://alarm-uk.us15.list-manage.com/subscribe/post?u=2dcc0ad9ad62b60e51cb6e8c2&amp;id=b4b0b652db" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
			    <div id="mc_embed_signup_scroll">
					<div class="agree">
						<div class="mc-field-group input-group">
						    <input type="checkbox" value="1" name="group[7597][1]" id="mce-group[7597]-7597-0" required>
						    <label for="mce-group[7597]-7597-0">I agree</label>
						</div>
					</div>
			    	<div class="signup">
						<div class="mc-field-group">
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email address" required>
							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_2dcc0ad9ad62b60e51cb6e8c2_b4b0b652db" tabindex="-1" value=""></div>
						    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Submit</button>
						</div>
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>
			    </div>
			</form>
		</div>
		<!--End mc_embed_signup-->
	</div>
	<div class="twitter">
		<a class="twitter-timeline" data-theme="light" data-link-color="#00a1e0" href="https://twitter.com/alarmrisk?ref_src=twsrc%5Etfw">Tweets by riskalarm</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	</div>
</section>

<footer id="main_footer">
	<div class="wrapper">

		<div class="left side">
			<a class="logo" href="/"><img src="<?php echo base_path().path_to_theme(); ?>/img/logo_white.png" alt="ALARM" title="ALARM">ALARM</a>
			<ul class="social">
				<li><a class="fb" href="https://www.facebook.com/alarmrisk/" target="_blank">Facebook</a></li>
				<li><a class="tw" href="https://twitter.com/ALARMrisk" target="_blank">Twitter</a></li>
				<li><a class="in" href="https://www.linkedin.com/groups/8534009" target="_blank">LinkedIn</a></li>
				<li><a class="ig" href="https://www.instagram.com/alarmrisk" target="_blank">Instagram</a></li>
			</ul>
		</div>

		<div class="right side">
			<?php print !empty($page['footer_menu']) ? render($page['footer_menu']) : ''; ?>
			<p>Design &amp; Build by <a href="https://www.r-evolution.co.uk/" target=-"_blank">r//evolution</a> <span>ALARM &copy; 2018</span></p>
		</div>

	</div>
</footer>

<section id="partners">
	<div class="wrapper">
		<div class="list">
			<h3>Our Platinum Sponsors</h3>
				<?php print !empty($page['footer_sponsors']) ? render($page['footer_sponsors']) : ''; ?>
		</div>
	</div>
</section>
