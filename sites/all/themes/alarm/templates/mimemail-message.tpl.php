<?php
/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php if ($css): ?>
            <style type="text/css">
                <?php print $css; ?>
            </style>
        <?php endif; ?>
    </head>
    <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>  marginheight="0" marginwidth="0" topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" style="padding:0px; margin:0px;">
        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main_wrapper" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
            <tr>
                <td valign="top" width="600">
                    <table class="inner_wrapper main_logo" width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                        <tr>
                            <td class="gutter" valign="top" align="left" width="40">&nbsp;</td>
                            <td class="content" valign="top" align="left" width="520">
                                <img src="https://www.alarmrisk.com/sites/all/themes/alarm/img/logo.png" alt="ALARM" width="275" height="54">
                            </td>
                            <td class="gutter" valign="top" align="left" width="40">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" width="600">
                    <table class="inner_wrapper" width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                        <tr>
                            <td class="content" valign="top" align="left" width="600">
                                <table class="inner_wrapper" width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                        <td class="gutter" valign="top" align="left" width="40">&nbsp;</td>
                                        <td class="content" valign="top" align="left" width="520">
                                            <?php print $body; ?>
                                        </td>
                                        <td class="gutter" valign="top" align="left" width="40">&nbsp;</td>
                                    </tr>
                                </table>
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>