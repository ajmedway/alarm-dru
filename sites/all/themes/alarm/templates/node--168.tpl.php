<?php

if (isset($content['body']))
  print render($content['body']);

?>

<?php if (($expiry = alarm_membership_get_membership_expiry_date('long')) !== false): ?>

  <p>Your membership expires on <strong><?= $expiry ?></strong></p>

  <p><a href="/membership-renewal-add-to-cart">Renew your membership</a></p>
  
<?php elseif (($lead_member = alarm_membership_get_lead_member_for_user(true)) !== false): ?>
  
  <p>Please contact your lead member, <strong><?= ucwords($lead_member->field_first_name->value() . ' ' . $lead_member->field_last_name->value()) ?></strong> about renewing your membership.</p>
  <p>We have the following email address on file for this member: <a href="mailto:<?= $lead_member->title->value() ?>" target="_blank"><?= $lead_member->title->value() ?></a></p>
  
<?php else: ?>
  
  <p>No membership details found.</p>
  
<?php endif; ?>

<?php
if (isset($content['field_downloads_listing']))
  print '<h3>Resources</h3>' . render($content['field_downloads_listing']);

if (isset($content['field_downloads']))
  print '<h3>Downloads</h3>' . render($content['field_downloads']);
?>

<?php $field_gallery_items = field_get_items('node', $node, 'field_gallery'); ?>

<?php if (!empty($field_gallery_items)) : ?>

  <h3>Gallery</h3>

  <div class="gallery">

      <?php foreach ($field_gallery_items as $item) : $imgUrl = file_create_url($item['uri']); ?>
        <a class="container" data-fancybox="gallery" href="<?php echo $imgUrl; ?>">
            <div class="image" style="background: url('<?php echo $imgUrl; ?>') center no-repeat;background-size:cover;"></div>
        </a>
      <?php endforeach; ?>

  </div>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>

<?php endif; ?>

<?php
if (isset($content['field_google_map_address']))
  print render($content['field_google_map_address']);

if (isset($content['field_youtube_video']))
  print render($content['field_youtube_video']);
?>
