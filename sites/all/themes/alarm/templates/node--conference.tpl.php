<?php
if (isset($content['field_main_image']))
  print render($content['field_main_image']);

if (isset($content['field_intro_text']))
  print '<h4>' . render($content['field_intro_text']) . '</h4>';

if (isset($content['body']))
  print render($content['body']);

// additionally render the conference event product for info purposes?
// ensure to hide the registration link on the conference_event product variant!
//if (isset($content['field_conference_events']))
  //print render($content['field_conference_events']);
?>

<?php
if (isset($content['field_downloads_listing']))
  print '<h3>Resources</h3>' . render($content['field_downloads_listing']);

if (isset($content['field_downloads']))
  print '<h3>Downloads</h3>' . render($content['field_downloads']);
?>

<?php $field_gallery_items = field_get_items('node', $node, 'field_gallery'); ?>

<?php if (!empty($field_gallery_items)) : ?>

  <h3>Gallery</h3>

  <div class="gallery">

      <?php foreach ($field_gallery_items as $item) : $imgUrl = file_create_url($item['uri']); ?>
        <a class="container" data-fancybox="gallery" href="<?php echo $imgUrl; ?>">
            <div class="image" style="background: url('<?php echo $imgUrl; ?>') center no-repeat;background-size:cover;"></div>
        </a>
      <?php endforeach; ?>

  </div>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>

<?php endif; ?>

<?php
if (isset($content['field_google_map_address']))
  print render($content['field_google_map_address']);

if (isset($content['field_youtube_video']))
  print render($content['field_youtube_video']);
?>

<div class="pricing_table">
    <!-- BEGIN OUTPUT from 'modules/system/region.tpl.php' -->
    <div class="region region-pricing">
        <!-- BEGIN OUTPUT from 'modules/block/block.tpl.php' -->
        <div id="block-views-event-tickets-block" class="block block-views">
            <h2>Book your place</h2>
            <div class="content">
                <!-- BEGIN OUTPUT from 'sites/all/modules/views/theme/views-view.tpl.php' -->
                <div class="view view-conference-tickets view-id-conference_tickets">
                    <div class="view-header">
                        <div id="tickets_info">
                            <p>
                                <strong>Places are free for full members. 50% discount applies for student, retired or unemployed members.</strong>
                                Please log in for your discount to apply. If you are not already a member, <a href="/membership" target="_blank">find out more here</a>.
                            </p>
                        </div>
                    </div>
                    <div class="view-content">
                        <div class="views-form">
                            <div class="row" data-row="0">
                                <p class="title">Conference Price</p>
                                <div class="quantity">
                                    <div class="form-item form-type-textfield form-item-add-to-cart-quantity-0">
                                        <input type="text" id="edit-add-to-cart-quantity-0" name="add_to_cart_quantity[0]" value="1" size="5" maxlength="128" class="form-text">
                                    </div>
                                    <p class="price"><?php print render($content['product:commerce_price'][0]['#markup']); ?></p>
                                </div>
                            </div>
                            <?php print render($content['field_product']); ?>
                        </div>
                    </div>
                </div>
                <!-- END OUTPUT from 'sites/all/modules/views/theme/views-view.tpl.php' -->
            </div>
        </div>
        <!-- END OUTPUT from 'modules/block/block.tpl.php' -->
    </div>
    <!-- END OUTPUT from 'modules/system/region.tpl.php' -->
</div>
