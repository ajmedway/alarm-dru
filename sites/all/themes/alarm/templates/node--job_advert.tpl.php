<h3>Job Description</h3>
<?php print render($content['field_job_description']); ?>
<h3>Salary</h3>
<p><?php print render($content['field_salary'][0]['#markup']); ?></p>
<h3>How to Apply</h3>
<?php print render($content['field_how_to_apply']); ?>
<?php if(isset($content['field_apply_link'])) print render($content['field_apply_link']); ?>