var j = jQuery.noConflict();
j(document).ready(function ($) {

    ////////////////////////////////////////////////////////////////////
    // Menu & Search:
    ////////////////////////////////////////////////////////////////////

    // Search:

    $(document).on("mouseover", ".search_btn", function () {

        $(this).find("input[type=text]").focus();

    }).on("mouseout", ".search_btn", function () {

        $(this).find("input[type=text]").blur();

    });

    // Mobile Menu:

    $(document).on("click", ".menu_btn", function () {

        $("body").toggleClass("menu_open");

    });


    ////////////////////////////////////////////////////////////////////
    // Slider:
    ////////////////////////////////////////////////////////////////////

    if ($(".front").length) {

        // Globals:

        var delay = 7500;
        var timeout = null;
        var animating = false;
        var slide_right = false;

        // Init:

        $("#main_slider .slide").each(function (i) {

            let slide = $(this);

            slide.attr("data-pos", (i + 2)).attr("data-slide", (i + 1));
            slide.css({background: slide.attr("data-color")});

        });

        $("#main_slider .image").each(function (i) {

            $(this).attr("data-pos", (i + 2)).attr("data-slide", (i + 1));
            $(this).find(".background").css({background: ("url(" + $(this).attr("data-url") + ") center no-repeat")});

            $("#main_slider .ticker").append('<div class="pip" data-pip="' + (i + 1) + '"></div>');

        });

        $("#main_slider .ticker .pip[data-pip=1]").addClass("active");

        // Controls:

        $("#main_slider .arrow").on("click", function () {

            clearTimeout(timeout);

            pagination($(this).hasClass("right") ? true : false);

        });

        // Logic:

        function pagination(slide_right) {

            if (!animating) {

                animating = true;

                if (!slide_right && !$("#main_slider .slide[data-pos=0]").length) {

                    $("#main_slider .slide[data-pos=" + $("#main_slider .slide").length + "]").attr("data-pos", 0);
                    $("#main_slider .image[data-pos=" + $("#main_slider .image").length + "]").attr("data-pos", 0);
                }

                setTimeout(function () {

                    $("#main_slider .slide").each(function () {

                        var pos = parseInt($(this).attr("data-pos"));
                        $(this).attr("data-pos", (slide_right ? pos - 1 : pos + 1));

                    });

                    $("#main_slider .image").each(function () {

                        var image = $(this).find(".image");
                        var pos = parseInt($(this).attr("data-pos"));
                        $(this).attr("data-pos", (slide_right ? pos - 1 : pos + 1));
                        $(this).next(".placeholder_image").attr("data-pos", (slide_right ? pos - 1 : pos + 1));

                    });

                    var current_pip = $("#main_slider .slide[data-pos=2]").attr("data-slide");

                    $("#main_slider .info .ticker .pip").removeClass("active");
                    $("#main_slider .info .ticker .pip[data-pip=" + current_pip + "]").addClass("active");

                }, 10);

                setTimeout(function () {

                    if ($("#main_slider .slide[data-pos=0]").length) {

                        $("#main_slider .slide[data-pos=0]").attr("data-pos", $("#main_slider .slide").length);
                        $("#main_slider .image[data-pos=0]").attr("data-pos", $("#main_slider .image").length);
                    }

                    animating = false;
                    nextSlide();

                }, 500);
            }
        }

        function nextSlide() {

            timeout = setTimeout(function () {
                pagination(true);
            }, delay);
        }

        nextSlide();
    }


    ////////////////////////////////////////////////////////////////////
    // Product Price Calculator:
    ////////////////////////////////////////////////////////////////////

    if ($("#block-views-event-tickets-block").length) {

        let rows = $("#block-views-event-tickets-block .row");
        let prices = [];

        rows.each(function (r) {

            let row = $(this);
            let price = row.find(".price");

            row.attr("data-row", r);
            prices[r] = price.text() == "Free" ? 0.00 : parseFloat(price.text().substring(1)).toFixed(2);

        });

        $(document).on("keyup", ".row", function () {

            let row = $(this);
            let r = parseInt(row.attr("data-row"));
            let price = row.find(".price");
            let quantity = row.find("input");

            if (quantity.val() < 0)
                quantity.val(0);

            prices[r] > 0
                    ? price.text("£" + (parseFloat(prices[r]) * quantity.val()).toFixed(2))
                    : price.text("Free");

        });
    }


    ////////////////////////////////////////////////////////////////////
    // Maps
    ////////////////////////////////////////////////////////////////////

    if ($("#map").length) {

        let map = null;

        function formatLatLng(raw_lat_lng) {

            raw_lat_lng = raw_lat_lng.split(", ");

            return {

                lat: parseFloat(raw_lat_lng[0]),
                lng: parseFloat(raw_lat_lng[1])
            };
        }

        function createMap() {

            let coords = {lat: 54.967164, lng: -1.594788};

            map = new GMaps({div: '#map', center: new google.maps.LatLng(coords.lat, coords.lng), lat: coords.lat, lng: coords.lng, zoom: 9, scrollwheel: false, styles: [{"featureType": "all", "elementType": "labels.text.fill", "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]}, {"featureType": "all", "elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "all", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#fefefe"}, {"lightness": 20}]}, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#dedede"}, {"lightness": 21}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}, {"lightness": 17}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 18}]}, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]}]});

            // Generate:

            $(".map_pin").each(function (current_pin) {

                let pin = $(this);
                let pos = formatLatLng(pin.attr("data-lat-lng"));

                pin.attr("data-id", current_pin);

                // Overlay version:

                map.drawOverlay({
                    verticalAlign: "top",
                    lat: pos.lat,
                    lng: pos.lng,
                    content: pin[0].outerHTML,
                    click: function () {}
                });

            });
        }

        createMap();

        // Show Info Box:

        $(document).on("click", "#map .map_pin .pin", function () {

            let pin = $(this).closest(".map_pin");

            // Make Visible:

            if (!pin.hasClass("visible")) {

                let coords = formatLatLng(pin.attr("data-lat-lng"));

                map.panTo(coords);

                $("#map .map_pin").removeClass("visible");
                pin.addClass("visible");

            } else {

                $("#map .map_pin").removeClass("visible");
            }

            // Update z-index Values:

            $("#map .map_pin").each(function () {

                let c_pin = $(this);
                let visible = c_pin.hasClass("visible");

                c_pin.parent("div").css({"z-index": (visible ? 150 : 100)});

            });

        });
    }


    ////////////////////////////////////////////////////////////////////
    // Board Member Profiles:
    ////////////////////////////////////////////////////////////////////

    if ($("#people_listing").length) {

        $("#people_listing .open_bio, #people_listing .modal").on("click", function () {

            $(this).closest(".member").find(".modal").toggleClass("open");

        });

        $("#people_listing .modal .inner").on("click", function (e) {

            e.stopPropagation();

        });
    }


    ////////////////////////////////////////////////////////////////////
    // Cart & Checkout:
    ////////////////////////////////////////////////////////////////////

    if ($(".page-checkout").length) {

        // Animated labels:

        var form_item = null;
        var form_selectors = ".page-checkout fieldset .form-item input, .page-checkout fieldset .form-item textarea";

        $(document).on("keydown", form_selectors, function (e) {

            form_item = $(this).parents(".form-item");

            if (!form_item.hasClass("has_value") && !form_item.hasClass("form-type-radio") && !form_item.hasClass("form-type-checkbox")
                    && e.keyCode !== 20 && e.keyCode !== 16 && e.keyCode !== 8 && e.keyCode !== 9) {
                form_item.addClass("has_value");
            }

        }).on("keyup", form_selectors, function () {

            if ($(this).val() === "")
                form_item.removeClass("has_value");

        });

        // Initialise:

        function initLabels() {

            $(".page-checkout fieldset .form-item input").each(function () {

                form_item = $(this).parents(".form-item");

                if ($(this).val() !== "" && !form_item.hasClass("form-type-radio") && !form_item.hasClass("form-type-checkbox")) {
                    form_item.addClass("has_value");
                }

            });
        }

        initLabels();

        $(".page-checkout").ajaxComplete(initLabels);

        // Membership Company Toggle:
        /*
        $(document).on("change", ".field-name-field-company select", function () {

            let details_fieldset = $(".group-company-details");

            $(this).val() != "_none"
                    ? details_fieldset.hide()
                    : details_fieldset.show();

        });
        */

        // Membership Company Toggle (Edit Form):

        $(document).on("change", "edit-field-company-und", function () {

            let details_fieldset = $(".field-name-field-company-address");

            $(this).val() != "_none"
                    ? details_fieldset.hide()
                    : details_fieldset.show();

        });

        // Checkout

        $("#edit-commerce-payment-payment-method label.option").text(function () {
            return $(this).text().replace("Payment via Worldpay", "Credit or Debit Card via WorldPay");
        });

        // Checkout Registration Page:

        if ($("#commerce-checkout-form-registration").length) {

            var $checkout_registration_form = $("#commerce-checkout-form-registration");

            // Map Email field to Title field:

            function mapEmailFieldToTitleField() {

                // get the registrant email address value
                let email = $(this).val();

                // traverse up to the root of the collapsible fieldset for registrant
                // and drill down to find the child title field input
                let $title_field = $(this).parents('#edit-registration-information fieldset.collapsible.member-registration-information-form').find(".content_member_title");

                // set registrant title field to match registrant email as it changes
                $title_field.val(email);
            }

            $(document).on("keyup change", ".anon_mail", mapEmailFieldToTitleField);
            $(document).ajaxComplete(mapEmailFieldToTitleField);

            // Manually validate membership registration member nodes all created
            // NOTE: This is now handled in a manual validation method in the alarm_membership module
//
//            var total_registrants = $('.member-registration-information-form  .field-type-entityreference.field-name-field-member').length;
//
//            function checkMemberRegistrantsAllCreated() {
//
//                // get the total number of member ief submit buttons found on the page
//                //let total_not_completed = $(".member-registration-information-form .ief-entity-submit").length;
//
//                // total completed member nodes found on page
//                let total_completed = $(".member-registration-information-form .inline-entity-form-node-title").length;
//
//                if (total_registrants > total_completed) {
//                    return false;
//                }
//
//                return true;
//            }
//
//            if (total_registrants > 0) {
//                
//                $checkout_registration_form.on("submit", function (e) {
//                    
//                    if (checkMemberRegistrantsAllCreated()) {
//                        return;
//                    }
//
//                    $checkout_registration_form.prepend('<div class="messages error">You must complete all membership registration form(s) and click on the "Create Member" button before you can proceed with checkout.</div>');
//                    e.preventDefault();
//
//                    $("body, html").animate({scrollTop: $checkout_registration_form.offset().top}, "slow");
//                    $(".checkout-processing").addClass("element-invisible");
//                });
//
//            }

        }

        // Toggle Date (ALARM Director Radios):

        let radio_group = ".field-name-field-alarm-director .form-type-radios ";
        let radios = $(radio_group).find("input[type=radio]");

        $(document).on("change", radios, function () {

            // ...

        });
    }

    ////////////////////////////////////////////////////////////////////
    // Membership Form (Update Details Profile Page):
    ////////////////////////////////////////////////////////////////////

    if ($(".node-content_member-form").length) {

        // Animated labels:

        var form_item = null;
        var form_selectors = ".node-content_member-form fieldset .form-item input, .node-content_member-form fieldset .form-item textarea";

        $(document).on("keydown", form_selectors, function (e) {

            form_item = $(this).parents(".form-item");

            if (!form_item.hasClass("has_value") && !form_item.hasClass("form-type-radio") && !form_item.hasClass("form-type-checkbox")
                    && e.keyCode !== 20 && e.keyCode !== 16 && e.keyCode !== 8 && e.keyCode !== 9)
                form_item.addClass("has_value");

        }).on("keyup", form_selectors, function () {

            if ($(this).val() === "")
                form_item.removeClass("has_value");

        });

        // Initialise:

        function initLabels() {

            $(form_selectors).each(function () {

                form_item = $(this).parents(".form-item");

                if ($(this).val() !== "" && !form_item.hasClass("form-type-radio") && !form_item.hasClass("form-type-checkbox"))
                    form_item.addClass("has_value");

            });
        }

        initLabels();

        $(".node-content_member-form").ajaxComplete(initLabels);

        // Membership Company Toggle:
        /*
        $(document).on("change", ".field-name-field-company select", function () {

            let details_fieldset = $(".group-company-details");

            $(this).val() != "_none"
                    ? details_fieldset.hide()
                    : details_fieldset.show();

        });
        */

        // Membership Company Toggle:

        $(document).on("change", "edit-field-company-und", function () {

            let details_fieldset = $(".field-name-field-company-address");

            $(this).val() != "_none"
                    ? details_fieldset.hide()
                    : details_fieldset.show();

        });

    }


    ////////////////////////////////////////////////////////////////////
    // Membership Options:
    ////////////////////////////////////////////////////////////////////

    if ($(".page-node-52").length) {

        var $full_membership_count = $(".membership-option-nid-25 .count");
        var $pricing = $full_membership_count.closest(".pricing");

        // create map of member numbers to corresponding product variant data
        var product_map = {
            1: {product_id: 14, price: 199},
            2: {product_id: 15, price: 388},
            3: {product_id: 17, price: 527},
            4: {product_id: 18, price: 666},
            5: {product_id: 19, price: 805},
            6: {product_id: 20, price: 944},
            7: {product_id: 21, price: 1063},
            8: {product_id: 22, price: 1182},
            9: {product_id: 23, price: 1301},
            10: {product_id: 24, price: 1420},
            11: {product_id: 25, price: 1539},
            12: {product_id: 26, price: 1658}
        };

        // set the max key (i.e. member number for product) in the product map
        var maxKey = 12;

        $full_membership_count.on("keyup change", function (e) {

            e.preventDefault();
            e.stopPropagation();

            let count = parseInt($full_membership_count.val());

            // interpolate all invalid selections to zero
            if (isNaN(count) || count <= 0) {
                count = 0;
                $(this).val('');
            }

            // prevent setting the members count to a value higher than we have a product for
            // e.g. the max product is for 12 members, so prevent setting to a number higher than 12
            if (!product_map.hasOwnProperty(count) && !isNaN(count) && count > maxKey) {
                count = maxKey;
                $(this).val(maxKey);
            }

            // default displayed price figures to 0
            let price = parseInt(0);
            let price_pp = parseInt(0);

            // calculate price figures if we have valid selections/data
            if (!isNaN(count) && !isNaN(price) && product_map.hasOwnProperty(count)) {
                price = parseInt(product_map[count].price);
                price_pp = parseInt(price / count);
            }

            $(this).text(count);
            $pricing.find(".price").text("£" + price);
            $pricing.find(".pp").text("(£" + price_pp + " PP)");
            $pricing.find(".people_word").text(count === 1 ? "person" : "people");

        });

        // handle full membership apply button clicks to submit the appropriate add to cart form
        $(document).on("click", "#apply-link-25-1", function (e) {

            e.preventDefault();

            let count = parseInt($full_membership_count.val());
            if (product_map.hasOwnProperty(count)) {
                if ($("#commerce-cart-add-to-cart-form-" + product_map[count].product_id).length) {
                    $("#commerce-cart-add-to-cart-form-" + product_map[count].product_id).submit();
                }
            }

        });

        // handle other membership apply button clicks to submit the appropriate add to cart form
        $(document).on("click", ".apply:not(#apply-link-25-1)", function (e) {

            e.preventDefault();

            // traverse up to the root of the collapsible fieldset for registrant
            // and drill down to find the child title field input
            let $member_product_add_to_cart_form = $(this).parents('.field-content').find(".commerce-add-to-cart");
            if ($member_product_add_to_cart_form.length) {
                $member_product_add_to_cart_form.submit();
            }

        });
    }


    ////////////////////////////////////////////////////////////////////
    // GDPR Update
    ////////////////////////////////////////////////////////////////////

    var gdpr_block = $("#block-views-gdpr-terms-version-block");
    var revisions = gdpr_block.find(".views-row");
    var version_number = revisions.length;

    var mc_form = $("#mc-embedded-subscribe-form");
    var mc_form_action = mc_form.attr("action");

    console.log(mc_form_action + "&GDPR=" + version_number);

    mc_form.attr("action", mc_form_action + "&GDPR=" + version_number);


    ////////////////////////////////////////////////////////////////////
    // General:
    ////////////////////////////////////////////////////////////////////

    // Make file downloads open in a new tab:

    $(".field-name-field-download a, .field-name-field-downloads .file a").each(function () {

        $(this).attr("target", "_blank");
    });

    // Exclude featured posts from clear both rule:

    if ($(".item[data-featured=Yes]").length) {

        $(".item[data-featured=Yes]").closest(".views-row").addClass("featured");
    }

    // Member Search:

    if ($("#members_search_listing #edit-combine").length) {

        $("#members_search_listing #edit-combine").attr("placeholder", "Search by name or email address...");
    }

    // Add attributes to Membership Export page view:

    if ($(".view-all-members-export-new").length) {

        $(".main_col").attr("id", "members_search_listing").find(".body").addClass("listing");
    }

    // make forum message more useful

    $('div.messages:contains("Content: Forum Topic")').
            append(" This may have been queued for approval.");

    // Replace cart with basket 

    $(".messages li a").text(function () {
        return $(this).text().replace("cart", "basket");
    });

    $(".view-header h1").text(function () {
        return $(this).text().replace("Cart", "Basket");
    });

    $(".cart_contents .fieldset-legend").text(function () {
        return $(this).text().replace("cart", "basket");
    });

    $(".checkout-review tr.pane-title td").text(function () {
        return $(this).text().replace("cart", "basket");
    });

    $('.region-pricing .form-actions input.form-submit').val(function (index, value) {
        return value.replace('Add to cart', 'Add to basket');
    });

    $('.commerce-cart-add-to-cart-form-7 input.form-submit').val(function (index, value) {
        return value.replace('Add to cart', 'Add to basket');
    });

});